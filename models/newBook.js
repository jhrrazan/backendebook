const mongoose = require('mongoose');

// Deklarasi subjudulSchema terlebih dahulu
const subjudulSchema = new mongoose.Schema({
  subjudul: {
    type: String,
    required: true
  },
  name: {
    type: String,
  },
  path: {
    type: String,
  }
});

module.exports = mongoose => {
  const newBookSchema = new mongoose.Schema({
    title: {
      type: String,
      required: false
    },
    subjudul: [subjudulSchema] // Mengacu pada skema subjudul
  });

  const NewBook = mongoose.model('NewBook', newBookSchema);
  return NewBook;
};
