const mongoose = require('mongoose');

module.exports = {
    mongoose,
    url : 'mongodb://127.0.0.1:27017/ebooks',
    ebook: require('./ebook')(mongoose),
    buku : require('./buku')(mongoose), 
    message : require('./message')(mongoose),
    PDF : require("./pdf")(mongoose),
    newBook : require("./newBook")(mongoose)
}