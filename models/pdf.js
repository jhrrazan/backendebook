const mongoose = require('mongoose');

module.exports = mongoose =>{
    const pdfSchema = new mongoose.Schema({
      name: String, // Original file name
      path: String, // File path
      title: String, // Book title
      // subtitle: String, // Subtitle
    });

    const PDF = mongoose.model("PDF", pdfSchema)
    return PDF
}
