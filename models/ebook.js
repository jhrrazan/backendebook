const mongoose = require('mongoose');

module.exports = mongoose => {

const ebookSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    file: {
        // data: name,  // Using Buffer to store binary data
        // contentType: String,  // MIME type of the file
        type: Number, // Number of bytes
        required: true, //


    }

}, {
    timestamp: true,
}

);
    const Ebook = mongoose.model('Ebook', ebookSchema);
    return Ebook;
}


