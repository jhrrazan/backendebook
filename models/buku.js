const mongoose = require('mongoose');

module.exports = mongoose => {
    const bukuSchema = new mongoose.Schema({
        nama : {
            type : String,
            required : true
        },
        halaman : {
            type : Number,
            required : true
        }
    }, {
        timestamps : true
    }
    )

    const Buku = mongoose.model('Buku', bukuSchema)
    return Buku
}