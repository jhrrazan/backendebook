const fs = require('fs');
const pdf = require('pdf-parse');

let dataBuffer = fs.readFileSync('topologi.pdf');

pdf(dataBuffer).then(function(data) {
  // data.text akan berisi seluruh teks dari PDF
  const text = data.text;
  
  // Gunakan regular expression untuk mencari "Bab 1", "Bab 2", dst.
  const babRegex = /Bab\s\d+/g;
  const babMatches = text.match(babRegex);
  
  if (babMatches) {
    console.log('Bab ditemukan:', babMatches);
    // Lakukan sesuatu dengan hasil pencarian
  } else {
    console.log('Bab tidak ditemukan');
  }
});
