module.exports = app => {
    const ebook = require('../controllers/ebookController');
    const buku = require('../controllers/bukuController');
    const router = require('express').Router();
    const pdfController = require('../controllers/pdfController');
    const newBookController = require('../controllers/newBookController'); // Pastikan path dan nama file benar

    const upload = require('../middleware/multer')
    const bodyParser = require('body-parser'); // Import body-parser
    const express = require('express');

    router.use(bodyParser.json());
    router.get('/ebooks', ebook.getAllEbooks);
    router.get('/ebooks/:id', ebook.getEbookById);
    router.post('/ebooks', ebook.createEbook);
    router.put('/ebooks/:id', ebook.updateEbook);
    router.delete('/ebooks/:id', ebook.deleteEbook);

    router.get('/books', buku.getAllBooks);
    router.post('/books', buku.createBook);
    router.get('/books/:id', buku.getBookById);
    router.put('/books/:id', buku.updateBook);
    router.delete('/books/:id', buku.deleteBook);

    router.post('/upload', upload.single('pdf'), pdfController.uploadPDF);

    router.post('/upload/bab', upload.single('pdf'), pdfController.uploadPDFbyBAB);

    router.get('/pdf/:id', pdfController.getPdfById);

    router.get('/pdf', pdfController.getAllPdf);

    // router.get('/pdf/bab', pdfController.getAllPdfSubtitle);

    router.get('/pdf/path/:id', pdfController.getPdfPathById);

    // Rute untuk mengambil URI PDF berdasarkan ID
    router.get('/pdf/getPdfPath/:id', pdfController.getPdfPathById);

    // Rute untuk mengambil URI PDF berdasarkan ID
    router.get('/pdf/getPdf/:id', pdfController.getPdf);

    router.get('/pdf/coba/:id/:name', pdfController.coba);

    router.get('/pdf/:id/:name', pdfController.getPdfByIdAndName);

    // Rute untuk membaca berkas PDF dengan ekstensi .pdf
    router.get('/tes/:name', pdfController.sendPDF);

    // router.put('/pdf/:id/update-title', pdfController.updatePDFTitle);

    // router.put('/pdf/:id/updateTitle', pdfController.updatePdfTitleById);

    router.delete('/pdf/:id', pdfController.deletePdfById);

    // Menambahkan rute untuk meng-update judul buku berdasarkan ID
    router.put('/pdf/updateTitle/:id', pdfController.updatePdfTitleById);

    // app.put('/pdf/updateTitleByOldTitle', pdfController.updatePdfTitleByOldTitle);

    // route baru dibawah

    router.post('/createbook', newBookController.createBook);
    router.get('/getAll', newBookController.getAllNewBook);
    router.get('/buku/:bookId/subjudul/:subjudulId', newBookController.getSubjudulById);
    router.get('/getSubjudul/:bookId', newBookController.getSubjudul);
    router.post('/subjudul/:id', upload.single('pdf'), newBookController.addSubjudul);
    router.delete('/buku/:bookId', newBookController.deleteJudul);
    router.delete('/books/:bookId/subjudul/:subjudulId', newBookController.deleteSubjudul);
    router.put('/updateBuku/:id', newBookController.updateJudul);
    router.put('/updateBuku/:bookId/subjudul/:subjudulId', newBookController.updateSubjudul);
    router.get('/pdfSubjudul/:subjudulId/:name', newBookController.getPdfBySubjudulIdAndName);
    router.get('/getSubjudul', newBookController.getAllSubjudul)
    
  
    module.exports = router;

    app.use('/api/', router);

}