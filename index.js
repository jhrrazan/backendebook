const express = require("express");
const app = express();
const http = require("http");
const cors = require("cors");
const server = http.createServer(app);
const db = require("./models");
// const bodyParser = require("body-parser");
const Message = db.message;
const { Server } = require("socket.io");  

// app.use(bodyParser.json()); // Middleware untuk mengurai body request dengan tipe 'application/json'
// app.use(bodyParser.urlencoded({ extended: true })); // Middleware untuk mengurai body request dengan tipe 'application/x-www-form-urlencoded'

const corsOptions = {
  origin: "*",
  methods: ["GET", "POST", "PUT", "DELETE"],
  origin: true, 
  credentials: true
}

app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

//register routes
require("./routes/api.routes")(app);




server.listen(3001, () => {
  console.log("listening on port:3001");
});