const db = require('../models');
const NewBook = db.newBook;
const path = require('path');
const fs = require('fs');


exports.createBook = async (req, res) => {
  try {
    const { title } = req.body;
    const newBook = new NewBook({ title, subjudul: [] });
    const savedBook = await newBook.save();
    res.status(201).send({ message: 'Buku berhasil dibuat', book: savedBook });
  } catch (error) {
    res.status(500).send({ message: 'Gagal membuat buku', error: error.message });
  }
};

exports.getAllNewBook = async (req, res) => {
  try {
    const newBook = await NewBook.find();
    res.status(200).json(newBook);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

exports.getSubjudulById = async (req, res) => {
  try {
    const { bookId, subjudulId } = req.params; // Mengambil ID buku dan ID subjudul dari URL
    
    // Temukan buku berdasarkan ID yang diberikan
    const existingBook = await NewBook.findById(bookId);

    // Cek apakah buku ditemukan
    if (!existingBook) {
      return res.status(404).send({ message: 'Buku tidak ditemukan' });
    }

    // Temukan subjudul berdasarkan ID yang diberikan
    const subjudul = existingBook.subjudul.find(sub => sub._id.toString() === subjudulId);

    // Cek apakah subjudul ditemukan dalam array
    if (!subjudul) {
      return res.status(404).send({ message: 'Subjudul tidak ditemukan' });
    }

    res.status(200).send({ message: 'Subjudul berhasil ditemukan', subjudul });
  } catch (error) {
    res.status(500).send({ message: 'Gagal mengambil subjudul', error: error.message });
  }
};

// Endpoint untuk mendapatkan subjudul berdasarkan ID buku
exports.getSubjudul = async (req, res) => {
  try {
    const { bookId } = req.params;
    const book = await NewBook.findById(bookId);

    if (!book) {
      return res.status(404).json({ message: 'Buku tidak ditemukan' });
    }

    // Ambil daftar subjudul dari buku yang sesuai
    const subjudulList = book.subjudul;

    res.status(200).json(subjudulList);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


exports.addSubjudul = async (req, res) => {
  try {
    const { id } = req.params; // Mengambil ID buku dari URL
    const { subjudul } = req.body;

    // Ambil informasi file yang diunggah dari request
    const { filename, path: filePath } = req.file;

    // Temukan buku berdasarkan ID yang diberikan
    const existingBook = await NewBook.findById(id);

    // Cek apakah buku ditemukan
    if (!existingBook) {
      return res.status(404).send({ message: 'Buku tidak ditemukan' });
    }

    // Tambahkan subjudul ke dalam array subjudul di buku yang ada
    existingBook.subjudul.push({ subjudul, name: filename, path: filePath });

    // Simpan perubahan ke dalam database
    const savedBook = await existingBook.save();

    res.status(201).send({ message: 'Subjudul berhasil ditambahkan', book: savedBook });
  } catch (error) {
    res.status(500).send({ message: 'Gagal menambahkan subjudul', error: error.message });
  }
};


exports.deleteJudul = async (req, res) => {
  try {
    const { bookId } = req.params; // Mengambil ID buku dari URL

    // Hapus buku berdasarkan ID yang diberikan
    const deletedBook = await NewBook.findByIdAndDelete(bookId);

    // Cek apakah buku berhasil dihapus
    if (!deletedBook) {
      return res.status(404).send({ message: 'Buku tidak ditemukan' });
    }

    res.status(200).send({ message: 'Buku berhasil dihapus', deletedBook });
  } catch (error) {
    res.status(500).send({ message: 'Gagal menghapus buku', error: error.message });
  }
};


exports.deleteSubjudul = async (req, res) => {
  try {
    const { bookId, subjudulId } = req.params; // Mengambil ID buku dan ID subjudul dari URL
    
    // Tambahkan console log untuk menampilkan nilai ID yang dikirimkan
    console.log('Book ID:', bookId);
    console.log('Subjudul ID:', subjudulId);

    // Temukan buku berdasarkan ID yang diberikan
    const existingBook = await NewBook.findById(bookId);

    // Cek apakah buku ditemukan
    if (!existingBook) {
      return res.status(404).send({ message: 'Buku tidak ditemukan' });
    }

    // Temukan indeks subjudul yang ingin dihapus
    const subjudulIndex = existingBook.subjudul.findIndex(
      (sub) => sub._id.toString() === subjudulId
    );

    // Cek apakah subjudul ditemukan dalam array
    if (subjudulIndex === -1) {
      return res.status(404).send({ message: 'Subjudul tidak ditemukan' });
    }

    // Hapus subjudul dari array
    existingBook.subjudul.splice(subjudulIndex, 1);

    // Simpan perubahan ke dalam database
    const savedBook = await existingBook.save();

    res.status(200).send({ message: 'Subjudul berhasil dihapus', book: savedBook });
  } catch (error) {
    res.status(500).send({ message: 'Gagal menghapus subjudul', error: error.message });
  }
};

exports.updateJudul = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari parameter URL
    const newTitle = req.body.newTitle; // Ambil judul baru dari body request

    // Pengecekan apakah ID dan judul tersedia
    if (!id) {
      return res.status(400).json({ error: 'ID tidak ditemukan' });
    }

    console.log('Request Body:', req.body);
    console.log('ID:', id);
    console.log('New Title:', newTitle);

    // Temukan buku berdasarkan ID dan perbarui judulnya
    const updatedNewBook = await NewBook.findByIdAndUpdate(id, { title: newTitle }, { new: true });

    if (!updatedNewBook) {
      return res.status(404).json({ error: 'Judul tidak ditemukan' });
    }

    res.status(200).json({ message: 'Judul buku berhasil di-update', updatedNewBook });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

exports.updateSubjudul = async (req, res) => {
  try {
    const { bookId, subjudulId } = req.params; // Ambil ID dari parameter URL
    const updatedSubjudul = req.body.updatedSubjudul; // Ambil informasi subjudul yang baru dari body request

    // Pengecekan apakah ID dan subjudul tersedia
    if (!bookId) {
      return res.status(400).json({ error: 'ID tidak ditemukan' });
    }

    if (!subjudulId) {
      return res.status(400).json({ error: 'Subjudul ID tidak ditemukan' });
    }

    // Temukan buku berdasarkan ID dan perbarui subjudulnya
    const updatedBook = await NewBook.findOneAndUpdate(
      { _id: bookId, 'subjudul._id': subjudulId },
      { $set: { 'subjudul.$.subjudul': updatedSubjudul } },
      { new: true }
    );

    if (!updatedBook) {
      return res.status(404).json({ error: 'Buku atau Subjudul tidak ditemukan' });
    }

    res.status(200).json({ message: 'Subjudul buku berhasil diperbarui', updatedBook });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Fungsi untuk mengambil dan mengirimkan berkas PDF berdasarkan ID subjudul dan nama PDF
exports.getPdfBySubjudulIdAndName = async (req, res) => {
  try {
    const { subjudulId, name } = req.params;

    console.log('Subjudul ID:', subjudulId);
    console.log('PDF Name:', name);

    // Temukan judul yang memiliki subjudul yang sesuai dengan ID yang diberikan
    const judul = await NewBook.findOne({ 'subjudul._id': subjudulId });

    if (!judul) {
      return res.status(404).json({ error: 'Judul tidak ditemukan' });
    }

    // Temukan berkas PDF berdasarkan nama yang diberikan di dalam judul yang sesuai
    const pdf = judul.subjudul.find(sub => sub._id.toString() === subjudulId && sub.name === name);

    if (!pdf) {
      return res.status(404).json({ error: 'Berkas PDF tidak ditemukan' });
    }

    // Dapatkan alamat berkas PDF
    const pdfPath = pdf.path;

    // Periksa apakah berkas tersebut memiliki ekstensi .pdf
    if (path.extname(pdfPath) !== '.pdf') {
      return res.status(400).json({ error: 'Berkas yang diminta bukan berkas PDF' });
    }

    // Baca berkas PDF menggunakan fs
    fs.readFile(pdfPath, (err, data) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: 'Terjadi kesalahan saat membaca berkas PDF' });
      }

      // Set header respons untuk tipe konten PDF
      res.setHeader('Content-Type', 'application/pdf');

      // Kirimkan berkas PDF ke respons
      res.send(data);
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};



exports.getAllSubjudul = async (req, res) => {
  try {
    const allSubjudul = await NewBook.aggregate([
      { $unwind: '$subjudul' }, // Pisahkan setiap subjudul menjadi dokumen tersendiri
      {
        $project: {
          _id: '$subjudul._id',
          name: '$subjudul.name',
          path: '$subjudul.path',
          subjudul: '$subjudul.subjudul'
        }
      } // Proyeksikan hanya properti yang diinginkan dari subjudul
    ]);

    res.status(200).json(allSubjudul);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};







// exports.addSubjudul = async (req, res) => {
//   try {
//     const { idJudul, subjudul } = req.body;

//     if (!idJudul || !subjudul) {
//       return res.status(400).json({ message: 'ID judul dan subjudul diperlukan' });
//     }

//     const foundBook = await NewBook.findById(idJudul);

//     if (!foundBook) {
//       return res.status(404).json({ message: 'Judul buku tidak ditemukan' });
//     }

//     foundBook.subjudul.push({ subjudul });
//     const updatedBook = await foundBook.save();

//     res.status(200).json({ message: 'Subjudul berhasil ditambahkan', judulBuku: updatedBook });
//   } catch (error) {
//     res.status(500).json({ message: 'Gagal menambah subjudul', error: error.message });
//   }
// };