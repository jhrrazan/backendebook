const db = require('../models');
const PDF = db.PDF
const path = require('path');
const fs = require('fs');

  // Upload file PDF
  exports.uploadPDF = async (req, res) => {
    try {
      if (!req.file) {
        return res.status(400).send('No PDF file uploaded.');
      }

      const { originalname, path } = req.file;
      const { title } = req.body; 

      const newPDF = new PDF({ name: originalname, path: path, title: title });

      await newPDF.save();
      res.status(201).send('PDF uploaded successfully with title: ' + title);
    } catch (error) {
      console.error(error);
      res.status(500).send('Error uploading PDF.');
    }
  };

  exports.uploadPDFbyBAB = async (req, res) => {
    try {
      if (!req.file) {
        return res.status(400).send('No PDF file uploaded.');
      }

      const { originalname, path } = req.file;
      const { subtitle } = req.body; 

      const newPDF = new PDF({ name: originalname, path: path, subtitle: subtitle });

      await newPDF.save();
      res.status(201).send('PDF uploaded successfully with subtitle: ' + subtitle);
    } catch (error) {
      console.error(error);
      res.status(500).send('Error uploading PDF.');
    }
  };

  //ambil semua buku
  exports.getAllPdf = async (req, res) => {
    try {
      const pdf = await PDF.find();
      res.status(200).json(pdf);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };

  // Fungsi untuk mengambil buku berdasarkan ID
  exports.getPdfById = async (req, res) => {
    try {
      const { id } = req.params; // Ambil ID dari parameter URL
      const buku = await PDF.findById(id);

      if (!buku) {
        return res.status(404).json({ error: 'Buku tidak ditemukan' });
      }

      res.status(200).json(buku);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };

  // Fungsi middleware untuk mengirimkan berkas PDF
  exports.sendPDF = (req, res) => {
    try {
      const pdfFilePath = path.join(__dirname, 'path_to_pdf_directory', req.params.name);

      // Periksa apakah berkas tersebut memiliki ekstensi .pdf
      if (path.extname(pdfFilePath) === '.pdf') {
        // Baca berkas PDF menggunakan fs
        fs.readFile(pdfFilePath, (err, data) => {
          if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Terjadi kesalahan saat membaca berkas PDF' });
          }

          // Set header respons untuk tipe konten PDF
          res.setHeader('Content-Type', 'application/pdf');

          // Kirimkan berkas PDF ke respons
          res.send(data);
        });
      } else {
        return res.status(400).json({ error: 'Berkas yang diminta bukan berkas PDF' });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };

 // Fungsi untuk mengambil URI PDF berdasarkan ID
exports.getPdfPathById = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari parameter URL
    const pdf = await PDF.findById(id);

    if (!pdf) {
      return res.status(404).json({ error: 'PDF tidak ditemukan' });
    }

    // Dapatkan nilai 'path' dari objek PDF
    const pdfPath = pdf.path; // Sesuaikan dengan atribut yang benar

    res.status(200).json({ path: pdfPath });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Fungsi untuk mengambil URI PDF berdasarkan ID
exports.getPdf = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari parameter URL
    const pdf = await PDF.findById(id);

    if (!pdf) {
      return res.status(404).json({ error: 'PDF tidak ditemukan' });
    }

    // Dapatkan nilai 'path' dari objek PDF
    const pdfPath = pdf.path; // Sesuaikan dengan atribut yang benar

    res.status(200).json({ path: pdfPath });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Fungsi untuk mengambil URI PDF berdasarkan ID
exports.coba = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari parameter URL
    const pdf = await PDF.findById(id);

    if (!pdf) {
      return res.status(404).json({ error: 'PDF tidak ditemukan' });
    }

    // Dapatkan nilai 'path' dari objek PDF
    const pdfPath = pdf.path; // Sesuaikan dengan atribut yang benar

    res.status(200).json({ path: pdfPath });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Fungsi untuk mengambil dan mengirimkan berkas PDF berdasarkan ID dan nama PDF
exports.getPdfByIdAndName = async (req, res) => {
  try {
    const { id, name } = req.params;

    // Cari berkas PDF berdasarkan ID
    const pdf = await PDF.findById(id);

    if (!pdf || pdf.name !== name) {
      return res.status(404).json({ error: 'Berkas PDF tidak ditemukan' });
    }

    // Dapatkan alamat berkas PDF
    const pdfPath = pdf.path;

    // Periksa apakah berkas tersebut memiliki ekstensi .pdf
    if (path.extname(pdfPath) !== '.pdf') {
      return res.status(400).json({ error: 'Berkas yang diminta bukan berkas PDF' });
    }

    // Baca berkas PDF menggunakan fs
    fs.readFile(pdfPath, (err, data) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: 'Terjadi kesalahan saat membaca berkas PDF' });
      }

      // Set header respons untuk tipe konten PDF
      res.setHeader('Content-Type', 'application/pdf');

      // Kirimkan berkas PDF ke respons
      res.send(data);
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Fungsi untuk menghapus PDF berdasarkan ID
exports.deletePdfById = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari parameter URL

    // Hapus dokumen PDF berdasarkan ID
    const deletedPdf = await PDF.findByIdAndDelete(id);

    if (!deletedPdf) {
      return res.status(404).json({ error: 'PDF tidak ditemukan' });
    }

    res.status(200).json({ message: 'PDF berhasil dihapus', deletedPdf });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

exports.updatePdfTitleById = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari parameter URL
    const newTitle = req.body.newTitle; // Ambil judul baru dari body request

    // Pengecekan apakah ID dan judul tersedia
    if (!id) {
      return res.status(400).json({ error: 'ID tidak ditemukan' });
    }


    console.log('ID:', id);
    console.log('New Title:', newTitle);

    // Temukan buku berdasarkan ID dan perbarui judulnya
    const updatedPdf = await PDF.findByIdAndUpdate(id, { title: newTitle }, { new: true });

    if (!updatedPdf) {
      return res.status(404).json({ error: 'PDF tidak ditemukan' });
    }

    res.status(200).json({ message: 'Judul buku berhasil di-update', updatedPdf });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};




// Fungsi untuk meng-update judul buku berdasarkan judul lama
exports.updatePdfTitleByOldTitle = async (req, res) => {
  try {
    const { oldTitle, newTitle } = req.body; // Ambil judul lama dan judul baru dari body request

    // Temukan buku berdasarkan judul lama dan perbarui judulnya
    const updatedPdf = await PDF.findOneAndUpdate(
      { title: oldTitle },
      { title: newTitle },
      { new: true }
    );

    if (!updatedPdf) {
      return res.status(404).json({ error: 'PDF tidak ditemukan' });
    }

    res.status(200).json({ message: 'Judul buku berhasil di-update', updatedPdf });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
