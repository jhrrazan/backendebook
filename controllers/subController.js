const db = require('../models');
const SUB = db.SUB
const path = require('path');
const fs = require('fs');

exports.uploadSUB = async (req, res) => {
    try {
      if (!req.file) {
        return res.status(400).send('No PDF file uploaded.');
      }

      const { originalname, path } = req.file;
      const { subtitle } = req.body; 

      const newPDF = new PDF({ name: originalname, path: path, subtitle: subtitle });

      await newPDF.save();
      res.status(201).send('PDF uploaded successfully with subtitle: ' + subtitle);
    } catch (error) {
      console.error(error);
      res.status(500).send('Error uploading PDF.');
    }
  };

  //ambil buku
  exports.getAllPdf = async (req, res) => {
    try {
      const sub = await SUB.find();
      res.status(200).json(sub);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };

  // Fungsi untuk mengambil buku berdasarkan ID
  exports.getPdfById = async (req, res) => {
    try {
      const { id } = req.params; // Ambil ID dari parameter URL
      const buku = await SUB.findById(id);

      if (!buku) {
        return res.status(404).json({ error: 'Buku tidak ditemukan' });
      }

      res.status(200).json(buku);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };