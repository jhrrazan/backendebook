const db = require('../models');
const Ebook = db.ebook;

// Get all ebooks
const getAllEbooks = async (req, res) => {
  try {
    const ebooks = await Ebook.find();
    res.status(200).json(ebooks);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Create a new ebook
const createEbook = async (req, res) => {
  try {
    const { name, file } = req.body;
    const newEbook = new Ebook({ name, file });
    const savedEbook = await newEbook.save();
    res.status(201).json(savedEbook);
    
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Get a single ebook by ID
const getEbookById = async (req, res) => {
  try {
    const ebook = await Ebook.findById(req.params.id);
    if (!ebook) {
      return res.status(404).json({ error: 'Ebook not found' });
    }
    res.status(200).json(ebook);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Update a single ebook by ID
const updateEbook = async (req, res) => {
  try {
    const updatedEbook = await Ebook.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true }
    );
    if (!updatedEbook) {
      return res.status(404).json({ error: 'Ebook not found' });
    }
    res.status(200).json(updatedEbook);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Delete a single ebook by ID
const deleteEbook = async (req, res) => {
  try {
    const deletedEbook = await Ebook.findByIdAndDelete(req.params.id);
    if (!deletedEbook) {
      return res.status(404).json({ error: 'Ebook not found' });
    }
    res.status(204).send();
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = {
  getAllEbooks,
  createEbook,
  getEbookById,
  updateEbook,
  deleteEbook,
};
