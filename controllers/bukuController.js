const db = require('../models')
const Buku = db.buku

// Get all books
const getAllBooks = async (req, res) => {
  try {
    const books = await Buku.find();
    res.status(200).json(books);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Create a new book
const createBook = async (req, res) => {
  try {
    const { nama, halaman } = req.body;
    const newBook = new Buku({ nama, halaman });
    const savedBook = await newBook.save();
    res.status(201).json(savedBook);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Get a single book by ID
const getBookById = async (req, res) => {
  try {
    const book = await Buku.findById(req.params.id);
    if (!book) {
      return res.status(404).json({ error: 'Book not found' });
    }
    res.status(200).json(book);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Update a single book by ID
const updateBook = async (req, res) => {
  try {
    const updatedBook = await Buku.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true }
    );
    if (!updatedBook) {
      return res.status(404).json({ error: 'Book not found' });
    }
    res.status(200).json(updatedBook);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Delete a single book by ID
const deleteBook = async (req, res) => {
  try {
    const deletedBook = await Buku.findByIdAndDelete(req.params.id);
    if (!deletedBook) {
      return res.status(404).json({ error: 'Book not found' });
    }
    res.status(204).send();
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = {
  getAllBooks,
  createBook,
  getBookById,
  updateBook,
  deleteBook,
};
